import unittest
import json
import os
import sys
sys.path.append(r"c:\newWork\socket")

from user_management import login, send

os.chdir('tests')


class TestUserManagement(unittest.TestCase):

    def setUp(self):
            users = {
                'greg1': {
                    'role':'admin',
                    'pass': 'ala',
                    'messages': [1,2,3]
                },
                'greg2': {
                    'role':'admin',
                    'pass': 'ala2',
                    'messages': []
                },
                'greg3': {
                    'role':'admin',
                    'pass': 'ala2',
                    'messages': [1]
                }
            }
            with open('test_users_data.json', 'w') as file:
                json.dump(users, file)

            self.file = 'test_users_data.json'

    #login(username, password, file):
    def test_login_returns_not_foud_user(self):
        self.assertEqual(login('greg21','pass', self.file), 'User does not exist')

    def test_login_returns_wrong_password(self):
        self.assertEqual(login('greg1','pass', self.file), 'Wrong password')

    def test_login_returns_successfull_user_profile(self):
        self.assertEqual(login('greg1','ala', self.file), {'role': 'admin', 'pass': 'ala', 'messages': [1, 2, 3]})

    #def send(username, message, file):
    def test_send_returns_sucesfull_send_message(self):
        self.assertEqual(send('greg1','Message 1', self.file), 'Message was sent')

    def test_send_returns_message_box_is_full(self):
        [send('greg1','Message 1', self.file) for _ in range(5)]
        self.assertEqual(send('greg1','Message 1', self.file), 'Message can not be send')


if __name__ == '__main__':
    unittest.main()