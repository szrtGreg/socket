from datetime import datetime
import socket
import json
import user_management 
import sys

VERSION = "v1.0"


server_start_time = datetime.now()

#SETTINGS
file = 'users_data.json'

commands = {
    'uptime': 'Server uptime',
    'info': 'Server version',
    'stop' : "Stops server and client",
    'help' : "Show all commands",
    'create <username> <password>': "Create user",
    'login <username> <password>': "Login user, display all data about user",
    'send <username> <message>': "Send message to specified user"
}


def help():
    return json.dumps(commands, indent=2).encode()

def uptime():
    time = (datetime.now() - server_start_time)
    uptime = {'uptime': f'{time}'}
    return json.dumps(uptime, indent=2).encode()

def info():
    info = {'info': f'{VERSION}'}
    return json.dumps(info, indent=2).encode()

def stop():
    info = {'stop': 'Server stops'}
    return json.dumps(info, indent=2).encode()

def create(username, password):
    user_management.create_new_user(username, password, file)
    info = {'create': f'User {username} created'}
    return json.dumps(info, indent=2).encode()

def login(username, password):
    info = {'login': user_management.login(username, password, file) }
    return json.dumps(info, indent=2).encode()

def send(username, message):
    info = {'send': user_management.send(username, message, file) }
    return json.dumps(info, indent=2).encode()

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind(('127.0.0.1', 65432))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print(f"Connected by {addr}")
        while True:
            data = conn.recv(1024).decode()
            command, *arg = data.split()
            if not data:
                break
            elif command == 'help':
                output = help()
                conn.sendall(output)
            elif command == 'uptime':
                output = uptime()
                conn.sendall(output)
            elif command == 'info':
                output = info()
                conn.sendall(output)
            elif command == 'stop' or command == 'exit':
                output = stop()
                conn.sendall(output)
                sys.exit()
            elif command == 'create':
                output = create(arg[0], arg[1])
                conn.sendall(output)
            elif command == 'login':
                output = login(arg[0], arg[1])
                conn.sendall(output)
            elif command == 'send':
                output = send(arg[0], arg[1:])
                conn.sendall(output)
            else:
                conn.sendall(b'not recognized command')